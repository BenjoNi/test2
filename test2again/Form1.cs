﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;
using System.IO;

using System.Collections;

namespace test2again
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        String filename = "CarPlace.xml";
        List<Car> c = new List<Car>();
        private void Form1_Load(object sender, EventArgs e)
        {
            loadFromFile();
        }

        private void loadFromFile()
        {
            listBox1.Items.Clear();
            TextReader tr = null;
            try
            {
                XmlSerializer x = new XmlSerializer(typeof(List<Car>));
                tr = new StreamReader(this.filename);
                c = (List<Car>)x.Deserialize(tr);

                setCurrentCar(c[c.Count - 1]);

                foreach (Car car in c)
                {
                    listBox1.Items.Add(car);
                }

                tr.Close();
            }
            catch (Exception xml) { MessageBox.Show(xml.Message); }
            if (tr != null) tr.Close();
        }

        private Car getCurrentCar()
        {
            Car b = new Car(); //defines new student variable
            b.Make = textBox1.Text;

            b.Model = textBox2.Text;

            b.MnfDate = dateTimePicker1.Value;
            b.Price = (double)numericUpDown1.Value;
            for (int i = 0; i < checkedListBox1.Items.Count; i++)
            {
                if (checkedListBox1.GetItemChecked(i))
                {
                    b.Options.Add(Option.available_options[checkedListBox1.Items[i].ToString()]);
                }
            }
            return b;
        }

        private void setCurrentCar(Car car)
        {

            textBox1.Text = car.Make;
            textBox2.Text = car.Model;
            dateTimePicker1.Value = car.MnfDate;
            numericUpDown1.Value = (int)car.Price;


            for (int i = 0; i < checkedListBox1.Items.Count; i++)
            {
                checkedListBox1.SetItemChecked(i, false);
            }

            foreach (Option option in car.Options)
            {
                for (int i = 0; i < checkedListBox1.Items.Count; i++)
                {
                    if (checkedListBox1.Items[i].ToString() == option.name)
                    {
                        checkedListBox1.SetItemChecked(i, true);
                    }
                }
            }

            updateTotal();
        }


        private void button1_Click(object sender, EventArgs e)
        {
            try {
                Car b = getCurrentCar();

                c.Add(b);// adds student b to student list c

                XmlSerializer x = new XmlSerializer(typeof(List<Car>));

                TextWriter writer = new StreamWriter(filename); //rewrites over existing file or creates a new one

                x.Serialize(writer, c); //serializes student list s 

                writer.Close(); //closes writer

                loadFromFile();
            } catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        private void updateTotal()
        {
            Car b = getCurrentCar();
            numericUpDown2.Value = (int)b.GetTotalPrice();
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            updateTotal();
        }

        private void checkedListBox1_MouseMove(object sender, MouseEventArgs e)
        {
            updateTotal();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            setCurrentCar((Car)listBox1.SelectedItem);
          
        }
    }

}
