﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test2again
{
    public class Option
    {
        public String name;
        public Double price;
        public Option(String _name, Double _price)
        {
            name = _name;
            price = _price;
        }
        public Option() { }
        public static Dictionary<string, Option> available_options = new Dictionary<String, Option>
        {
            {"Air condition", new Option("Air condition", 1000) },
            {"Airbag", new Option("Airbag", 500) },
            {"Seat warmers", new Option("Seat warmers", 1500) }
        };
    }

}
