﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test2again
{
    public class Car
    {
        public Car()
        {
            Options = new List<Option>();
        }
        public String Make;
        public String Model;
        public DateTime MnfDate;
        public Double Price;
        public List<Option> Options;

        public override string ToString()
        {
            return Make + " - " + Model;
        }

        public Double GetTotalPrice()
        {
            Double t = Price;
            foreach (Option option in Options)
            {
                t += option.price;
            }
            return t;
        }
    }
}
